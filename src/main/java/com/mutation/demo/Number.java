package com.mutation.demo;

import org.springframework.stereotype.Component;

@Component
public class Number {

    public boolean isPositive(int number) {
        return number >= 0;
    }

}