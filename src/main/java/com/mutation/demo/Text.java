package com.mutation.demo;

import org.springframework.stereotype.Component;

@Component
public class Text {

    public int countCharts(String text){
        var count = 0;
        if(text == null)
            throw new NullPointerException("El valor enviado es nulo.");
        else
            count = text.length();
        return count;
    }

}
