package com.mutation.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class TextTest {

    @Autowired
    private Text text;

    @Test
    void countChartsTest() {
        Assertions.assertEquals(8, text.countCharts("Selenium"), "La cantidad de caracteres no coincide.");
    }

}