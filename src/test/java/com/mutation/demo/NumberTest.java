package com.mutation.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class NumberTest {

    @Autowired
    private Number numbers;

    @Test
    void isPositiveTest() {
        Assertions.assertTrue(numbers.isPositive(10), "El número no es positivo.");

    }

}
